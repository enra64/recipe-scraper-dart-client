# swagger
This is a basic server that makes the python library recipe-scrapers available as REST endpoints.

This Dart package is automatically generated by the [Swagger Codegen](https://github.com/swagger-api/swagger-codegen) project:

- API version: 1.0.0
- Build package: io.swagger.codegen.v3.generators.dart.DartClientCodegen

## Requirements

Dart 1.20.0 or later OR Flutter 0.0.20 or later

## Installation & Usage

### Github
If this Dart package is published to Github, please include the following in pubspec.yaml
```
name: swagger
version: 1.0.0
description: Swagger API client
dependencies:
  swagger:
    git: https://github.com/GIT_USER_ID/GIT_REPO_ID.git
      version: 'any'
```

### Local
To use the package in your local drive, please include the following in pubspec.yaml
```
dependencies:
  swagger:
    path: /path/to/swagger
```

## Tests

TODO

## Getting Started

Please follow the [installation procedure](#installation--usage) and then run the following:

```dart
import 'package:swagger/api.dart';
// TODO Configure HTTP basic authorization: basicAuth
//swagger.api.Configuration.username = 'YOUR_USERNAME';
//swagger.api.Configuration.password = 'YOUR_PASSWORD';

var api_instance = new ConversionApi();
var body = new RecipeRequest(); // RecipeRequest | Recipe URL

try {
    var result = api_instance.convertRecipe(body);
    print(result);
} catch (e) {
    print("Exception when calling ConversionApi->convertRecipe: $e\n");
}
```

## Documentation for API Endpoints

All URIs are relative to *http://localhost:8020*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*ConversionApi* | [**convertRecipe**](docs//ConversionApi.md#convertrecipe) | **PUT** /recipe | Request the conversion of a recipe

## Documentation For Models

 - [RecipeRequest](docs//RecipeRequest.md)
 - [RecipeResponse](docs//RecipeResponse.md)

## Documentation For Authorization


## basicAuth

- **Type**: HTTP basic authentication


## Author

anoweb+recipe-server@posteo.de
