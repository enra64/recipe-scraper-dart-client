part of swagger.api;

class RecipeRequest {
  
  String url = null;

  RecipeRequest();

  @override
  String toString() {
    return 'RecipeRequest[url=$url, ]';
  }

  RecipeRequest.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    url = json['url'];
  }

  Map<String, dynamic> toJson() {
    return {
      'url': url
     };
  }

  static List<RecipeRequest> listFromJson(List<dynamic> json) {
    return json == null ? new List<RecipeRequest>() : json.map((value) => new RecipeRequest.fromJson(value)).toList();
  }

  static Map<String, RecipeRequest> mapFromJson(Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, RecipeRequest>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) => map[key] = new RecipeRequest.fromJson(value));
    }
    return map;
  }
}
