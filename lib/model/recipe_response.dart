part of swagger.api;

class RecipeResponse {
  
  String image = null;

  String name = null;

  String instructions = null;

  double prepTime = null;

  double cookTime = null;

  double totalTime = null;

  double ratingCount = null;

  double ratingValue = null;

  String author = null;

  List<String> ingredients = [];

  RecipeResponse();

  @override
  String toString() {
    return 'RecipeResponse[image=$image, name=$name, instructions=$instructions, prepTime=$prepTime, cookTime=$cookTime, totalTime=$totalTime, ratingCount=$ratingCount, ratingValue=$ratingValue, author=$author, ingredients=$ingredients, ]';
  }

  RecipeResponse.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    image = json['image'];
    name = json['name'];
    instructions = json['instructions'];
    prepTime = json['prepTime'];
    cookTime = json['cookTime'];
    totalTime = json['totalTime'];
    ratingCount = json['ratingCount'];
    ratingValue = json['ratingValue'];
    author = json['author'];
    ingredients = (json['ingredients'] as List).map((item) => item as String).toList();
  }

  Map<String, dynamic> toJson() {
    return {
      'image': image,
      'name': name,
      'instructions': instructions,
      'prepTime': prepTime,
      'cookTime': cookTime,
      'totalTime': totalTime,
      'ratingCount': ratingCount,
      'ratingValue': ratingValue,
      'author': author,
      'ingredients': ingredients
     };
  }

  static List<RecipeResponse> listFromJson(List<dynamic> json) {
    return json == null ? new List<RecipeResponse>() : json.map((value) => new RecipeResponse.fromJson(value)).toList();
  }

  static Map<String, RecipeResponse> mapFromJson(Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, RecipeResponse>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) => map[key] = new RecipeResponse.fromJson(value));
    }
    return map;
  }
}
