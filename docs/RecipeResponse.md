# swagger.model.RecipeResponse

## Load the model package
```dart
import 'package:swagger/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**image** | **String** |  | [optional] [default to null]
**name** | **String** |  | [optional] [default to null]
**instructions** | **String** |  | [optional] [default to null]
**prepTime** | **double** |  | [optional] [default to null]
**cookTime** | **double** |  | [optional] [default to null]
**totalTime** | **double** |  | [optional] [default to null]
**ratingCount** | **double** |  | [optional] [default to null]
**ratingValue** | **double** |  | [optional] [default to null]
**author** | **String** |  | [optional] [default to null]
**ingredients** | **List&lt;String&gt;** |  | [optional] [default to []]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

