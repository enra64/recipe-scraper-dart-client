# swagger.api.ConversionApi

## Load the API package
```dart
import 'package:swagger/api.dart';
```

All URIs are relative to *http://localhost:8020*

Method | HTTP request | Description
------------- | ------------- | -------------
[**convertRecipe**](ConversionApi.md#convertRecipe) | **PUT** /recipe | Request the conversion of a recipe

# **convertRecipe**
> RecipeResponse convertRecipe(body)

Request the conversion of a recipe

### Example
```dart
import 'package:swagger/api.dart';
// TODO Configure HTTP basic authorization: basicAuth
//swagger.api.Configuration.username = 'YOUR_USERNAME';
//swagger.api.Configuration.password = 'YOUR_PASSWORD';

var api_instance = new ConversionApi();
var body = new RecipeRequest(); // RecipeRequest | Recipe URL

try {
    var result = api_instance.convertRecipe(body);
    print(result);
} catch (e) {
    print("Exception when calling ConversionApi->convertRecipe: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**RecipeRequest**](RecipeRequest.md)| Recipe URL | 

### Return type

[**RecipeResponse**](RecipeResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

